extends Node2D

const WIDTH: int = 320
const HEIGHT: int = 200


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		# Charge la scène du jeu
		pass
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit(0)

func _init() -> void:
#	OS.window_fullscreen = true
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


func _ready() -> void:
	yield(get_tree().create_timer(1), "timeout")

	
# Called when the node enters the scene tree for the first time.
func _draw() -> void:
	var delayHud: float = 0
	var durationHud: float = 3
	var delay: float = 0
	var duration: float = 2

	# Configuration pour l'écran
	$FadeIn.interpolate_property($HUD, "modulate:a", -0.3,1, duration, Tween.TRANS_LINEAR, Tween.EASE_IN,delay)
	delay+=duration*2
	$FadeIn.start()
	$FadeIn.repeat=false

	$HUD/Present.modulate.a = 0
	# Configure le fondu pour le label
	$HUD/Tween.interpolate_property($HUD/Present, "modulate:a", -0.3,1, durationHud, Tween.TRANS_LINEAR, Tween.EASE_IN,delayHud)
	delayHud+=durationHud
	$HUD/Tween.start()
	$HUD/Tween.repeat=false
	$Music.play()

func _on_Timer_timeout() -> void:
	var delay: float = 0
	var duration: float = 0.5
	# Configure le fondu pour l'écran
	delay+=duration
	$FadeOut.interpolate_property($HUD, "modulate:a", 1,-0.3, duration, Tween.TRANS_LINEAR, Tween.EASE_IN,delay)

	$FadeOut.start()
	$FadeOut.repeat=false

func _on_FadeOut_tween_completed(_object: Object, _key: NodePath) -> void:
	OS.delay_msec(200)
	# Adapte le viewport aux bonne dimensions
	OS.set_window_size(Vector2(WIDTH,HEIGHT))
	get_viewport().set_size_override(true,Vector2(WIDTH,HEIGHT),Vector2(0,0))
	$"/root".size=Vector2(WIDTH,HEIGHT)
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT, SceneTree.STRETCH_ASPECT_KEEP, Vector2(WIDTH,HEIGHT))

	var _res = get_tree().change_scene_to(load("res://SceneRV/Test/Test.tscn"))
